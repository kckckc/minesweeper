import { flagTile, revealTile } from './actions';
import { clearAnimation } from './animation';
import { getNeighbors } from './helper';
import { MINE_DENSITY, TILE_MARGIN_X, TILE_MARGIN_Y, TILE_SIZE } from './main';
import { gameState, grid, TileData } from './state';

export const resetBoard = () => {
	const app = document.querySelector<HTMLDivElement>('#app')!;

	// Reset
	app.innerHTML = '';
	grid.splice(0, grid.length);
	clearAnimation();
	gameState.frozen = false;

	// Calculate size
	const { width, height } = app.getBoundingClientRect();
	gameState.tileWidth = Math.floor(width / TILE_SIZE) - TILE_MARGIN_X;
	gameState.tileHeight = Math.floor(height / TILE_SIZE) - TILE_MARGIN_Y;

	// Build grid
	for (let y = 0; y < gameState.tileHeight; y++) {
		const rowDiv = document.createElement('div');
		rowDiv.style.display = 'flex';
		rowDiv.style.flexDirection = 'row';
		rowDiv.style.justifyContent = 'center';

		const rowData = [];

		for (let x = 0; x < gameState.tileWidth; x++) {
			const tileDiv = document.createElement('div');
			tileDiv.style.width = TILE_SIZE + 'px';
			tileDiv.style.height = TILE_SIZE + 'px';
			tileDiv.classList.add('tile');

			tileDiv.addEventListener('click', (e) => {
				if (e.button === 0) {
					revealTile(x, y);
				}
			});
			tileDiv.addEventListener('contextmenu', (e) => {
				flagTile(x, y);
				e.preventDefault();
			});

			rowDiv.appendChild(tileDiv);

			const tileData = {
				x,
				y,
				hidden: true,
				bomb: false,
				flagged: false,
				exploded: false,
				locked: false,
				el: tileDiv,
			};
			rowData.push(tileData);
			updateTileElement(tileData);
		}

		app.appendChild(rowDiv);

		grid.push(rowData);
	}

	populateBoard();
};

export const updateTileElement = (tile: TileData) => {
	const { x, y, hidden, flagged, exploded, el } = tile;
	if (hidden) {
		el.classList.add('hidden');

		el.innerHTML = '';
	} else {
		el.classList.remove('hidden');

		el.innerHTML = getNeighbors(tile).filter((t) => t.bomb).length + '';
	}
	if (flagged) {
		el.classList.add('flagged');
	} else {
		el.classList.remove('flagged');
	}
	if (exploded) {
		el.classList.add('exploded');
	} else {
		el.classList.remove('exploded');
	}

	el.style.borderWidth =
		(y > 0 ? '1px' : '0px') +
		' ' +
		(x < gameState.tileWidth - 1 ? '1px' : '0px') +
		' ' +
		(y < gameState.tileHeight - 1 ? '1px' : '0px') +
		' ' +
		(x > 0 ? '1px' : '0px');
};

const populateBoard = () => {
	const numMines = Math.round(
		gameState.tileWidth * gameState.tileHeight * Math.min(MINE_DENSITY, 1.0)
	);
	gameState.numBombs = numMines;
	let placedMines = 0;

	while (placedMines < numMines) {
		const mx = Math.floor(Math.random() * gameState.tileWidth);
		const my = Math.floor(Math.random() * gameState.tileHeight);

		const tile = grid[my][mx];

		if (!tile.bomb) {
			tile.bomb = true;
			placedMines++;
		}
	}
};
