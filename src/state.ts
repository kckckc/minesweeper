export type TileData = {
	x: number;
	y: number;
	hidden: boolean;
	bomb: boolean;
	flagged: boolean;
	exploded: boolean;
	locked: boolean;
	el: HTMLDivElement;
};

export const grid: TileData[][] = [];

export const gameState = {
	numBombs: 0,

	tileWidth: 0,
	tileHeight: 0,

	wins: 0,
	losses: 0,

	frozen: false,
};

let auto = JSON.parse(localStorage.getItem('setting-auto') ?? 'false');
export const getAuto = () => {
	return auto;
};
export const setAuto = (value: boolean) => {
	auto = value;
	localStorage.setItem('setting-auto', value.toString());
};
