import { playNextAnimation, queueAnimation } from './animation';
import { resetBoard, updateTileElement } from './board';
import { checkIsFirstMove, checkWin, getNeighbors } from './helper';
import { onLoss } from './main';
import { gameState, getAuto, grid } from './state';

export const flagTile = (x: number, y: number) => {
	if (gameState.frozen) return;

	const tile = grid[y][x];
	if (!tile) return;

	tile.locked = false;

	if (!tile.hidden) return;

	tile.flagged = !tile.flagged;
	updateTileElement(tile);

	checkWin();
};

export const revealTile = (x: number, y: number) => {
	if (gameState.frozen) return;

	const tile = grid[y][x];
	if (!tile) return;

	tile.locked = false;

	if (!tile.hidden) return;
	if (tile.flagged) return;

	if (tile.bomb) {
		if (checkIsFirstMove()) {
			resetBoard();
			revealTile(x, y);
			return;
		}

		tile.exploded = true;
		updateTileElement(tile);
		onLoss();
		return;
	}

	tile.hidden = false;
	updateTileElement(tile);

	// Flood fill 0s
	if (!getAuto()) {
		const neighbors = getNeighbors(tile);
		if (neighbors.filter((t) => t.bomb).length === 0) {
			neighbors.forEach((n) => {
				queueAnimation(() => {
					if (n.hidden) {
						revealTile(n.x, n.y);
					} else {
						playNextAnimation();
					}
				});
			});
		}
	}

	checkWin();
};
