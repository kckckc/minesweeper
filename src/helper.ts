import { onWin } from './main';
import { gameState, getAuto, grid, TileData } from './state';

export const getTile = (x: number, y: number) => {
	return (grid[y] ?? [])[x] ?? null;
};

export const getNeighbors = (tile: TileData) => {
	const neighbors: TileData[] = [];

	for (let y = -1; y < 2; y++) {
		for (let x = -1; x < 2; x++) {
			if (x == 0 && y == 0) continue;

			const t = getTile(tile.x + x, tile.y + y);
			if (t) {
				neighbors.push(t);
			}
		}
	}

	return neighbors;
};

export const checkSolved = () => {
	let correct = 0;
	let allRevealed = true;
	grid.forEach((row) =>
		row.forEach((tile) => {
			if (tile.flagged && tile.bomb) {
				correct++;
			}
			if (tile.hidden && !tile.flagged) {
				allRevealed = false;
			}
		})
	);

	// In auto mode, click everything so it looks nice
	if (getAuto() && !allRevealed) {
		return false;
	}

	return correct === gameState.numBombs;
};

export const checkWin = () => {
	if (checkSolved()) {
		onWin();
	}
};

export const checkIsFirstMove = () => {
	let isFirst = true;
	grid.forEach((row) => {
		row.forEach((tile) => {
			if (!tile.hidden || tile.flagged) {
				isFirst = false;
			}
		});
	});

	return isFirst;
};
