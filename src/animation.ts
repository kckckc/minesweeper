import { runAi } from './auto';
import { ANIMATION_SPEED, MINE_DENSITY } from './main';
import { gameState, getAuto, grid } from './state';

const animationQueue: (() => void)[] = [];

export const queueAnimation = (fn: () => void) => {
	animationQueue.push(fn);
};

export const clearAnimation = () => {
	animationQueue.splice(0, animationQueue.length);
};

export const playNextAnimation = () => {
	const cb = animationQueue.shift();
	if (cb) {
		cb();
	} else {
		// If nothing is happening and we're in auto mode,
		// step the ai
		if (getAuto()) {
			runAi();
		}
	}
};

const statsDiv = document.getElementById('stats')!;

const EMDASH = '—';

export const startAnimationLoop = () => {
	setInterval(() => {
		// Stop playing after we lost
		if (!gameState.frozen) {
			playNextAnimation();
		}

		// Update stats text
		let flagged = 0;
		grid.forEach((row) => {
			row.forEach((tile) => {
				if (tile.flagged) {
					flagged++;
				}
			});
		});

		const mineDensityPercent = MINE_DENSITY * 100;
		const winPercent =
			Math.round(
				(gameState.wins / (gameState.wins + gameState.losses)) * 10000
			) / 100;

		statsDiv.innerHTML = [
			`${gameState.tileWidth} x ${gameState.tileHeight} @ ${mineDensityPercent}%`,
			`${flagged}/${gameState.numBombs} flagged`,
			`${gameState.wins}W ${gameState.losses}L (${
				isNaN(winPercent) ? 0 : winPercent
			}%)`,
			`auto: ${getAuto() ? 'on' : 'off'} (press A)`,
			`press R to reset`,
		]
			.map((e, i, a) => (i < a.length - 1 ? [e, EMDASH] : e))
			.flat()
			.join(' ');
	}, ANIMATION_SPEED);
};
