import { startAnimationLoop } from './animation';
import { resetBoard } from './board';
import { gameState, getAuto, setAuto } from './state';
import './style.css';

export const TILE_SIZE = 40; // 32 / 48
export const TILE_MARGIN_X = 3; //1;
export const TILE_MARGIN_Y = 2;

export const MINE_DENSITY = 0.15;

export const ANIMATION_SPEED = 30; // 50
export const LOSS_RESET_DELAY = 1000;
export const WIN_RESET_DELAY = 2000;

export const onWin = () => {
	// alert('you won');
	gameState.wins += 1;
	setTimeout(() => {
		resetBoard();
	}, WIN_RESET_DELAY);
};

export const onLoss = () => {
	// alert('you died'); //TODO
	gameState.losses += 1;
	gameState.frozen = true;
	setTimeout(() => {
		resetBoard();
	}, LOSS_RESET_DELAY);
};

resetBoard();
startAnimationLoop();

// Reset after resize
let resizeDebounce: ReturnType<typeof setTimeout> | null = null;
window.addEventListener('resize', () => {
	if (resizeDebounce) {
		clearTimeout(resizeDebounce);
	}

	resizeDebounce = setTimeout(() => {
		resetBoard();
	}, 200);
});

// r -> reset
// a -> toggle auto
window.addEventListener('keydown', (e) => {
	if (e.key === 'r') {
		gameState.losses++;
		resetBoard();
	} else if (e.key === 'a') {
		setAuto(!getAuto());
		console.log('auto: ' + getAuto());
	}
});
