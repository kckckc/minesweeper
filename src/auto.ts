import { flagTile, revealTile } from './actions';
import { queueAnimation } from './animation';
import { checkIsFirstMove, checkSolved, getNeighbors, getTile } from './helper';
import { gameState, getAuto, grid } from './state';

export const runAi = () => {
	if (!getAuto()) return;

	const isSolved = checkSolved();
	if (isSolved) return;

	let actionTaken = false;

	if (checkIsFirstMove()) {
		queueRevealRandomTile();
		actionTaken = true;
	}

	// let locked = 0;
	// grid.forEach((row) => {
	// 	row.forEach((tile) => {
	// 		if (tile.locked) {
	// 			locked++;
	// 		}
	// 	});
	// });
	// console.log('auto pass, locked:', locked);

	grid.forEach((row) => {
		row.forEach((tile) => {
			if (tile.hidden) return;

			const neighbors = getNeighbors(tile);

			const numBombs = neighbors.filter((n) => n.bomb).length;
			const numFlagged = neighbors.filter((n) => n.flagged).length;
			const numHidden = neighbors.filter(
				(n) => !n.flagged && n.hidden
			).length;

			if (numBombs === numFlagged) {
				neighbors
					.filter((n) => !n.locked && !n.flagged && n.hidden)
					.forEach((n) => {
						n.locked = true;
						actionTaken = true;
						queueAnimation(() => {
							revealTile(n.x, n.y);
						});
					});
			}

			if (numHidden === numBombs - numFlagged) {
				neighbors
					.filter((n) => !n.locked && !n.flagged && n.hidden)
					.forEach((n) => {
						n.locked = true;
						actionTaken = true;
						queueAnimation(() => {
							flagTile(n.x, n.y);
						});
					});
			}
		});
	});

	if (!actionTaken) {
		queueRevealRandomTile();
	}
};

export const queueRevealRandomTile = () => {
	const rx = Math.floor(Math.random() * gameState.tileWidth);
	const ry = Math.floor(Math.random() * gameState.tileHeight);

	const t = getTile(rx, ry);
	if (!t.hidden || t.flagged) {
		queueRevealRandomTile();
		return;
	}

	t.locked = true;
	queueAnimation(() => revealTile(rx, ry));
};
